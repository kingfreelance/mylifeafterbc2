<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function login(){
        $data['page_title'] = "Content Management";
		$this->form_validation->set_rules('user', 'User', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run() === FALSE){
            if(!$this->session->userdata('logged_in')){
                $data['content'] = "admin/login";
                $this->load->view("template/layout", $data);
            }else{
                redirect(base_url('admin/home'));
            }
        } else {
            $username = $this->input->post('user');
            $password = md5($this->input->post('password'));
            $user = $this->admin_model->login($username, $password);
            if($user){
                $user_data = array(
                    'user_id' => $user->id,
                    'email' => $user->username,
                    'name' => 'Administrator',
                    'logged_in' => true
                );
                $this->session->set_userdata($user_data);
                $this->session->set_flashdata('message', 'Logged in successfull');
                redirect(base_url('admin/home'));
            } else {
                $this->session->set_flashdata('message', 'Login Failed');
                $data['content'] = "admin/login";
                $this->load->view("template/layout", $data);
            }
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        $this->session->set_flashdata('message', 'Signed out');
        redirect(base_url('admin/login'));
    }
    
    public function home(){
        $data['page_title'] = "Content Management";
        $data['content'] = "admin/home";
        $this->load->view("template/layout", $data);
    }

    public function assets(){
        $data['page_title'] = "Content Management";
        $data['content'] = "admin/assets";
        $data['details'] = $this->admin_model->query("SELECT * FROM tbl_web_assets where id = 1");
        $this->load->view("template/layout", $data);
    }

    public function save_assets(){
        $this->form_validation->set_rules('site_banner', 'Site Banner', 'required');
        $this->form_validation->set_rules('personal_signature', 'Personal Signature', 'required');
        if($this->form_validation->run()){
            $insert_data = array(
                "site_banner"  => $this->input->post('site_banner'),
                "personal_signature"  => $this->input->post('personal_signature')
 
            );
            $this->admin_model->update('tbl_web_assets', $insert_data, 'id = 1');
            $this->session->set_flashdata('message', 'Records successfuly saved');
        } else {
            $this->session->set_flashdata('message', 'Please fill all the required fields.');
        }

        redirect(base_url('admin/assets'));
    }


    public function links(){
        $data['page_title'] = "Content Management";
        $data['content'] = "admin/links";
        $data['details'] = $this->admin_model->query("SELECT * FROM tbl_web_links where id = 1");
        $this->load->view("template/layout", $data);
    }

    public function save_links(){
        $this->form_validation->set_rules('facebook', 'Facebook', 'required');
        $this->form_validation->set_rules('instagram', 'Instagram', 'required');
        $this->form_validation->set_rules('youtube', 'youtube', 'required');
        if($this->form_validation->run()){
            $insert_data = array(
                "facebook"  => $this->input->post('facebook'),
                "instagram"  => $this->input->post('instagram'),
                "youtube"  => $this->input->post('youtube')
 
            );
            $this->admin_model->update('tbl_web_links', $insert_data, 'id = 1');
            $this->session->set_flashdata('message', 'Records successfuly saved');
        } else {
            $this->session->set_flashdata('message', 'Please fill all the required fields.');
        }

        redirect(base_url('admin/links'));
    }
}
