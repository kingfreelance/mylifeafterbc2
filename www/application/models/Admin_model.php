<?php
	class Admin_model extends CI_Model{
		public function __construct(){
			$this->load->database();
		}
		public function login($username, $password){
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$result = $this->db->get('tbl_users');
			if($result->num_rows() == 1){
				return $result->row(0);
			} else {
				return false;
			}
		}

		public function query($query){
			return $this->db->query($query)->result();
		}

		public function save($table, $data){
			$this->db->insert($table, $data);
			return $this->db->affected_rows();
		}



		public function update($table, $data, $query){
			$this->db->set($data);
			$this->db->where($query);
			$this->db->update($table);
			return $this->db->affected_rows();
		}

		public function self_url($title){
			$search = array(" ","ö","ü","ı","ğ","ç","ş","/","?","&","'",",","A","B","C","Ç","D","E","F","G","Ğ","H","I","İ","J","K","L","M","N","O","Ö","P","R","S","Ş","T","U","Ü","V","Y","Z","Q","X");
			$replace = array("-","o","u","i","g","c","s","-","","-","","","a","b","c","c","d","e","f","g","g","h","i","i","j","k","l","m","n","o","o","p","r","s","s","t","u","u","v","y","z","q","x");
			$new_text = str_replace($search,$replace,trim($title));
			return $new_text;
		}
	}