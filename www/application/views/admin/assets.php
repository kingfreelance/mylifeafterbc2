<?php $this->load->view("admin/navbar"); ?>
<div class="container_body">
    
    <?php echo form_open('admin/assets/save'); ?> 
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="admin-page-title">Website Assets</h4>
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>

        <?php 
            if($this->session->flashdata('message')){
                echo "<div class='alert alert-warning alert-dismissible'><button type='button' class='close' data-dismiss='alert'>&times;</button>".$this->session->flashdata('message')."</div>"; 
            }
        ?>
        <div class="form-group">
            <label class="col-md-12 control-label">Site Banner</label>  
            <div class="col-md-12 inputGroupContainer">
                <div class="input-group">
                    <span id="preview_personal_signature" class=" input-group-addon">
                        <i class="glyphicon glyphicon-picture"></i>
                    </span>
                    <input  id="site_banner" name="site_banner" placeholder="Site Banner" class="form-control"  type="text" onclick="openFileManager('site_banner','site_banner_preview')" value="<?= @$details[0]->site_banner;?>">
                </div>
                <img id="site_banner_preview" src="<?= @$details[0]->site_banner;?>" class="filemanage_preview" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12 control-label">Signature</label>  
            <div class="col-md-12 inputGroupContainer">
                <div class="input-group">
                    <span id="preview_personal_signature" class=" input-group-addon">
                        <i class="glyphicon glyphicon-picture"></i>
                    </span>
                    <input  id="personal_signature" name="personal_signature" placeholder="Personal Signature" class="form-control"  type="text" onclick="openFileManager('personal_signature','personal_signature_preview')" value="<?= @$details[0]->personal_signature;?>">
                </div>
                <img id="personal_signature_preview" src="<?= @$details[0]->personal_signature;?>" class="filemanage_preview" />
            </div>
        </div>
    <?php echo form_close(); ?>  
</div>


<script>
function openFileManager(elementid, previewid){
    var url = "/assets/fileman/index.html?integration=custom&type=files&txtFieldId=" + elementid + "&previewID=" + previewid;
    $('#fileManagerPanel').dialog({modal:true, width:875,height:600});
    $("#fileManagerFrame").attr("src",url);
}
function closeCustomRoxy2(){
    $('#fileManagerPanel').dialog('close');
}
</script>

<div id="fileManagerPanel" style="display: none;">
  <iframe id="fileManagerFrame" src="" style="width:100%;height:100%" frameborder="0">
  </iframe>
</div>