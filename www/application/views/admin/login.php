<div class="row">
    <div class="Absolute-Center is-Responsive">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <?php echo form_open('admin/login',["class"=>"w3-container"]); ?> 
                <h4 id="logo-container">Admin Login</h4>
                <?php 
                    if($this->session->flashdata('message')){
                        echo "<div class='alert alert-warning alert-dismissible'><button type='button' class='close' data-dismiss='alert'>&times;</button>".$this->session->flashdata('message')."</div>"; 
                    }
                ?>
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input class="form-control" type="text" name='user' placeholder="username"/>          
                </div>
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input class="form-control" type="password" name='password' placeholder="password"/>     
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                </div>
            <?php echo form_close(); ?>        
        </div>  
    </div>    
</div>