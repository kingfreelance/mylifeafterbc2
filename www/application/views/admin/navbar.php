<?php
    $details= $this->admin_model->query("SELECT * FROM tbl_web_assets where id = 1");
?>
<div class="navbar-div">
    <img src="<?= $details[0]->site_banner;?>" width="400px;" />
    <ul class="nav">
        <li>
            <a href="#">Home</a>
        </li>
        <li class="button-dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle"> Website<span>▼</span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?= base_url("admin/assets");?>">Assets</a>
                <li>
                
                <li>
                    <a href="<?= base_url("admin/links");?>">Links</a>
                <li>
                <li>
                    <a href="#">Web Quote</a>
                <li>
            </ul>
        </li>
        <li>
            <a href="#">My Story</a>
        </li>
        <li>
            <a href="#">Journey to Healing</a>
        </li>
        <li class="button-dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle"> Healthy Cooking Recipes<span>▼</span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#">Categories</a>
                <li>
                
                <li>
                    <a href="#">Recipes</a>
                <li>
            </ul>
        </li>
        <li class="button-dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle"> Random Ramblings<span>▼</span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#">Categories</a>
                <li>
                
                <li>
                    <a href="#">Articles</a>
                <li>
            </ul>
        </li>
        <li>
            <a href="<?= base_url("admin/logout");?>">Logout</a>
        </li>
    </ul>
</div>
<script style="text/javascript">
jQuery(document).ready(function (e) {
    function t(t) {
        e(t).bind("click", function (t) {
            t.preventDefault();
            e(this).parent().fadeOut()
        })
    }
    e(".dropdown-toggle").click(function () {
        var t = e(this).parents(".button-dropdown").children(".dropdown-menu").is(":hidden");
        e(".button-dropdown .dropdown-menu").hide();
        e(".button-dropdown .dropdown-toggle").removeClass("active");
        if (t) {
            e(this).parents(".button-dropdown").children(".dropdown-menu").toggle().parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
        }
    });
    e(document).bind("click", function (t) {
        var n = e(t.target);
        if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-menu").hide();
    });
    e(document).bind("click", function (t) {
        var n = e(t.target);
        if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-toggle").removeClass("active");
    })
});
</script>