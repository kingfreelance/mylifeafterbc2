<?php $this->load->view("admin/navbar"); ?>
<div class="container_body">
    
    <?php echo form_open('admin/links/save'); ?> 
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="admin-page-title">Website Links</h4>
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>

        <?php 
            if($this->session->flashdata('message')){
                echo "<div class='alert alert-warning alert-dismissible'><button type='button' class='close' data-dismiss='alert'>&times;</button>".$this->session->flashdata('message')."</div>"; 
            }
        ?>
        <div class="form-group">
            <label class="col-md-12 control-label">Facebook</label>  
            <div class="col-md-12 inputGroupContainer">
                <div class="input-group">
                    <span id="preview_personal_signature" class=" input-group-addon">
                        <i class="glyphicon glyphicon-globe"></i>
                    </span>
                    <input name="facebook" placeholder="Facebook" class="form-control"  type="text" value="<?= @$details[0]->facebook;?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12 control-label">Instagram</label>  
            <div class="col-md-12 inputGroupContainer">
                <div class="input-group">
                    <span id="preview_personal_signature" class=" input-group-addon">
                        <i class="glyphicon glyphicon-globe"></i>
                    </span>
                    <input name="instagram" placeholder="Instagram" class="form-control"  type="text" value="<?= @$details[0]->instagram;?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12 control-label">Youtube</label>  
            <div class="col-md-12 inputGroupContainer">
                <div class="input-group">
                    <span id="preview_personal_signature" class=" input-group-addon">
                        <i class="glyphicon glyphicon-globe"></i>
                    </span>
                    <input name="youtube" placeholder="Youtube" class="form-control"  type="text" value="<?= @$details[0]->youtube;?>">
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>  
</div>

