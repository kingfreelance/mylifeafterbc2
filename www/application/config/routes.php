<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['admin'] = 'admin/login';
$route['admin/home'] = 'admin/home';
$route['admin/logout'] = 'admin/logout';
$route['admin/assets'] = 'admin/assets';
$route['admin/links'] = 'admin/links';

$route['admin/assets/save'] = 'admin/save_assets';
$route['admin/links/save'] = 'admin/save_links';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
